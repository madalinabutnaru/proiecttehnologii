Nume echipa: KeepITFresh;
Componenta echipa: Butnaru Madalina si Constantin Alina Marina;
Tema aleasa: 8. Manager de restaurante integrat cu GoogleMaps;

1.Descrierea aplicatiei
Problema rezolvata
     Principala problema pe care proiectul nostru o abordeaza este cea a gestionarii timpului. Utilizatorii pot vedea exact distantele pana la restaurantele din zona sau cele preferate, pot face rezervare si pot comanda. Tema aleasa ne va ajuta sa proiectam o aplicatie prin care utilizatorii  pot gasi cele mai apropiate restaurante de locul in care se afla sau fata de o adresa predefinita. Vor putea fi redirectionati direct spre pagina restaurantelor din apropiere pentru a putea vizualiza meniul. Acestea pot fi grupate in functie de locatie (sector, zona) sau in functie de specificul restaurantului (chinezesc, libanez, fast-food etc.)

Utilizatori
     In sens larg, proiectul se adreseaza oricarei persoane, indiferent de varsta, care doreste sa gaseasca un restaurant in zona. Proiectul este adresat in special managerilor de restaurant sau antreprenorilor care doresc sa isi deschida o astfel de afacere, putand prin aceasta aplicatie sa gaseasca zone nepopulate de restaurante sau zone bine cotate in acest domeniu.

Aplicatii asemanatoare
     Aplicatii care folosesc acelasi principiu sunt: RestoBYT, HipMenu, FoodSpoting, FoodPanda.

2.Interfetele aplicatiei
     Pentru dezvoltarea aplicatiei ne vom folosi de css, html si javascript. 
Metode expuse de backend
     In momentul in care se deschide aplicatia, utilizatorul va trebui sa completeze zona in care doreste sa gaseasca restaurantele dorite si orasul. Dupa care, acesta va fi redirectionat catre o alta pagina in care ii vor fi afisate restaurantele din zona respectiva in functie de distanta. De asemenea, utilizatorul va avea posibilitatea sa selecteze specificul restaurantului, dar si tipul de mancare dorit, precum desert, paste, peste etc. 
     Dupa selectarea restaurantului, acestuia ii vor aparea o serie de informatii utile, precum programul localului, meniul si posibilitatea de a face o rezervare sau de a da o comanda.
     Dupa ce a selectat ceea ce doreste, poate face o rezervare oferind informatiile: nume, prenume, numar de telefon si numar de persoane. Pentru comanda, informatiile vor fi mai numeroase.
     Componente relevante ale aplicatiei:
-lista de restaurante;
-lista de categorii;
-metode de plata;
-totalul comenzii;
-pozitionarea pe harta a restaurantelor.

API REST
Parametrii de request:
GET /restaurants;
GET/categories;
GET/categories?name{paramName};
GET/restaurants?name={restaurantName}&orderBy={orderParam};
POST/orders.
 Actiunile utilizatorului
-vizualizarea restaurantelor dintr-o anumita zona;
-poate folosi bara de search pentru a cauta un anumit local;
-poate plasa o comanda;
-poate face o rezervare.

 


